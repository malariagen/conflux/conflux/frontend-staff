# Sanger Workflow Frontend

## Build Setup

```bash
# Create env file
$ cp .env.example .env

[Set the HOST to 'localhost' (or your actual IP, if it's authorised), and 
NUXT_ENV_OAUTH_CLIENT_ID from zoho vault, or add NUXT_ENV_SKIP_AUTH=true]

# Create .npmrc file
$ cp .npmrc.example .npmrc

[Enter FontAwesome token in .npmrc]

# install dependencies
$ npm install

# serve with hot reload at localhost:4200
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Docker Environment Setup

The local docker environment has been provided using a `docker-compose.yml`
file. It comes with an optional Mock environment provided by Swagger and using
the OpenAPI contract. 

```bash
# Create env file
$ cp .env.example .env

# serve with hot reload at localhost:4200
$ docker-compose up -d

# check logs for the running node server
$ docker-compose logs -f app

# Run tests
$ docker-compose run --rm app npm run test

# Run selenium tests
$ docker-compose -f docker-compose.yml -f docker-compose.selenium.yml up -d selenium
$ docker-compose -f docker-compose.yml -f docker-compose.selenium.yml up tests
```

> When running the selenium tests, you will need to set the `TEST_USER_USERNAME` and
>`TEST_USER_PASSWORD` variables in your `.env` file. The values for this can be found
> in the Gitlab CI pipeline configuration for the application.

### Mock Environment

The mock environment is available to view at http://localhost:4300 and uses the docker
image from the [api-contract](https://gitlab.com/malariagen/conflux/api-contract) repository. This 
provides the OpenAPI contract that both the backend and frontend must follow and uses
Swagger to automatically mock it for the frontend app to use. 

#### Access

You may not be able to access the registry where the docker image for the mock is held, 
in such a case you may need to login. 

    docker login registry.gitlab.com

You will be asked for your Gitlab.com username and password. If you are using 2-factor
authentication for your Gitlab.com account, you will not be able to use your normal 
password, and will instead need to generate an access token to use as a password instead. 

https://gitlab.com/profile/personal_access_tokens

Once you have generated an access token with the appropriate permissions you can use
that as your password when logging in using the command above. 

#### Version

By default the version of the OpenApi contract that is used in the local environment will
be the latest built version. However, you can change the version by altering the `API_VERSION`
environment variable in your `.env` file. You can pick from whatever version appears in
the [api-contract registry](https://gitlab.com/malariagen/conflux/api-contract/container_registry).

## Adding Environment Variables

Add the environment variable (and optionally an example value) to `.env.example` and `.env`.

### Server Side

Add the environment variable to server/config.js, with an optional default value:

```serverEnvVar: process.env.SERVER_ENV_VAR```

Use the environment variable with `require('./server/config').serverEnvVar`.

This is to ensure that the call to `require('dotenv')` is not duplicated.

### Client Side

Client side variables should begin with `NUXT_ENV_`, and will be available on the
`window.env` object. See `client/app.html` to see how this is done.

## Integration testing

As the Sanger API is quite slow, there are mocked responses to the API routes built in to the nuxt
server, e.g. http://localhost:4200/api/studies. This allows integration tests to run quickly.


## Adding Components

Components can be added to this repository, but ideally will be added to our shared MalariaGEN
components repository: https://gitlab.com/malariagen/conflux/frontend-component-library

Once the component has been added there, you can use this command to pull the updated library:

```
npm i sanger-components --force
```


## Docker Multistage Build

The Dockerfile for this project will create a docker image for this project using
multistage builds to keep the size of the final image to a minimum and allow
sensible caching of layers to speed up rebuilding an image. 

To build the image using the existing Dockerfile using the following command

    docker build -t malaria-gen/workflow:12-alpine .
    
The full build of the docker image as now using the `node:12-alpine` base image
is `271MB`, which is compared to `1.09GB` when using the Debian base image
`node:12`.

### Build Environment Variables

If the build step needs environment variables to complete, this can be done in the
following ways.

**Static Environment Variables**

These are environment variables that won't change depending on environment and are safe
to bake into the Dockerfile itself, but you don't want to specify in the application.

```dockerfile
FROM node:12-alpine AS build

ENV MY_BUILD_VAR=some_static_value

```

If an environment variable might change more often, or if it is a secret that 
shouldn't be baked into the Dockerfile, then you will need to do this. 

```dockerfile
FROM node:12-alpine AS build

ARG MY_BUILD_ARG=default_value
ENV MY_BUILD_VAR=$MY_BUILD_ARG

```

You will then need to run the `docker build` command will then need to be run like
so 

    docker build --build-arg MY_BUILD_ARG=some_value -t malaria-gen/workflow:12-alpine .
    
If it is a secure value, you will want to have set an environment variable in your 
bash terminal so that you can refer to that 

    source .secure_env_vars
    docker build --build-arg MY_BUILD_ARG=$SECURE_VAR -t malaria-gen/workflow:12-alpine .
    
If an environment variable will be different between environments, then you should not 
define it at build time, but instead only define it at runtime when you run the docker
container. 

    docker run --rm -p 5000:5000 -e MY_RUNTIME_VAR=some_value malaria-gen/workflow:12-alpine

You can do this using the `nuxt-env` module defined in the `nuxt.config.js` file
