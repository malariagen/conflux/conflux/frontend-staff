import Vue from 'vue'
import sangerApi from '../services/sangerApi'

export const state = () => ({})

export const mutations = {
  setModel(state, { name, model }) {
    Vue.set(state, name, model)
  }
}

export const actions = {
  async loadModel({ commit }, name) {
    try {
      const model = await sangerApi.getModel(name)
      commit('setModel', { name, model })
    } catch (e) {
      commit('setModel', { name, model: null })
    }
  }
}
