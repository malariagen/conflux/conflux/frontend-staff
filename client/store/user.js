import { getProfile } from '../services/oauth'

export const state = () => ({ profile: null })

export const mutations = {
  setProfile(state, profile) {
    state.profile = profile
  }
}

export const actions = {
  async loadProfile({ commit }) {
    try {
      const profile = await getProfile()
      commit('setProfile', profile)
    } catch (e) {
      commit('setProfile', null)
    }
  }
}
