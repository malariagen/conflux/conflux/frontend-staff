import { createTableStore } from '@outlandish/sanger-components'
import ApiDataLoader from '../services/apiDataLoader'

export default createTableStore(new ApiDataLoader('samples'))
