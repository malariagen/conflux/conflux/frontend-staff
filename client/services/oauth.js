import axios from 'axios'

export const getLoginUrl = () => {
  const redirectUrl = encodeURIComponent(
    `${window.location.protocol}//${window.location.host}/oauth/callback`
  )
  return `https://${window.env.NUXT_ENV_SSO_SERVICE_DOMAIN}/sso/oauth2.0/authorize?response_type=token&client_id=${window.env.NUXT_ENV_OAUTH_CLIENT_ID}&redirect_uri=${redirectUrl}&scope=editor`
}

export const getProfile = async () => {
  const response = await axios.get(
    `https://${window.env.NUXT_ENV_SSO_SERVICE_DOMAIN}/sso/oauth2.0/profile`,
    {
      headers: {
        Authorization: `Bearer ${window.localStorage.accessToken}`
      }
    }
  )
  if (!response || !response.data) {
    throw new Error('Error getting profile')
  }
  return response.data
}
