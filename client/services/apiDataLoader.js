import sangerApi from '../services/sangerApi'

class ApiDataLoader {
  constructor(endpoint) {
    this.endpoint = endpoint
  }

  fetch(filters, offset, limit, orderBy, includeSummary, requestId) {
    return sangerApi.getData(
      this.endpoint,
      filters,
      offset,
      limit,
      orderBy,
      includeSummary,
      requestId
    )
  }
}

export default ApiDataLoader
