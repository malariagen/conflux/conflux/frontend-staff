import axios from 'axios'

const get = (path, params) =>
  axios.get(`${window.env.NUXT_ENV_API_SERVICE_URL}${path}`, {
    headers: {
      Authorization: `Bearer ${window.localStorage.accessToken}`
    },
    params
  })

/* Use default export for easier jest mocking */
export default {
  getModel: async (model) => {
    const response = await get(`/model/${model}`)
    if (!response || !response.data) {
      throw new Error(`Error getting model ${model}`)
    }
    return response.data
  },
  getData: async (endpoint, filters, offset, limit) => {
    const response = await get(`/${endpoint}`, { filters, offset, limit })
    if (!response || !response.data) {
      throw new Error(`Error getting data from ${endpoint}`)
    }
    return response.data
  }
}
