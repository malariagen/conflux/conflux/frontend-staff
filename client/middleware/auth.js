import { getLoginUrl } from '../services/oauth'
/**
 * Catch unauthenticated users attempting to visit protected routes,
 * and redirect them back to '/'.
 *
 * To mark a route as "public", add this property to the page object:
 *
 *     meta: {
 *       public: true
 *     }
 *
 * See pages/oauth/callback.vue for an example.
 */
export default async ({ store, redirect, route }) => {
  if (window.env.NUXT_ENV_SKIP_AUTH === 'true') {
    window.localStorage.accessToken = 'fake_auth_token'
    return
  }
  // route.meta === [{ public: true }]
  if (route.meta.find((item) => item.public)) {
    return
  }
  // check the user's access token is present and active
  await store.dispatch('user/loadProfile')
  // if the profile was not successfully loaded, redirect
  if (!store.state.user.profile) {
    return redirect(getLoginUrl())
  }
}
