const baseConfig = require('./wdio.conf')

baseConfig.config.services.push('chromedriver')

// Uncomment this line to test a single test file
// baseConfig.config.specs = ['./webdriverio/specs/studies.js']

module.exports = baseConfig
