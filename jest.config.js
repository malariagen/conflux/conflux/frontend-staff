const path = require('path')

module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/client/$1',
    '^~/(.*)$': '<rootDir>/client/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest'
  },
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/client/components/**/*.vue',
    '<rootDir>/client/pages/**/*.vue'
  ],
  coverageDirectory: 'test/coverage',
  reporters: [
    'default',
    [
      'jest-html-reporter',
      {
        outputPath: path.join(__dirname, 'test', 'unit-test-report.html')
      }
    ]
  ]
}
