const axios = require('axios')
const config = require('../config')

module.exports = {
  sendMessage: async (message) => {
    await axios.post(config.slackWebhookUrl, {
      text: message
    })
  }
}
