const slack = require('./slack')

process
  .on('unhandledRejection', async (reason, p) => {
    // eslint-disable-next-line no-console
    console.error('Unhandled Rejection at', p, reason)
    await slack.sendMessage(`Unhandled Rejection ${String(reason)}`)
  })
  .on('uncaughtException', async (err) => {
    // eslint-disable-next-line no-console
    console.error('Uncaught', err)
    await slack.sendMessage(`Uncaught ${String(err)}`)
    process.exit(1)
  })
