require('dotenv').config()

module.exports = {
  // General
  nodeEnv: process.env.NODE_ENV || 'production',

  // Server
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 5000,

  // Nuxt
  description: 'Conflux Staff App',

  // Slack
  slackWebhookUrl: process.env.SLACK_WEBHOOK_URL
}
