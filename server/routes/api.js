const express = require('express')
const router = express.Router()

router.use((req, res, next) => {
  const { authorization } = req.headers
  if (!authorization || !authorization.includes('Bearer')) {
    return res.sendStatus(403)
  }
  const token = authorization.split('Bearer')[1].trim()
  if (!token) {
    return res.sendStatus(403)
  }
  next()
})

/**
 * Mocked for E2E testing (the SIMS API is too slow)
 */
router.get('/studies', (req, res) => {
  res.send({
    studies: [
      { name: 'Study 1', code: 1 },
      { name: 'Study 2', code: 2 }
    ]
  })
})

module.exports = router
