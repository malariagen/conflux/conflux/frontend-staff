const express = require('express')
const { Nuxt, Builder } = require('nuxt')
const bodyParser = require('body-parser')
const nuxtConfig = require('../nuxt.config.js')
const config = require('./config')

// Import and Set Nuxt.js options
nuxtConfig.dev = config.nodeEnv !== 'production'

const start = async () => {
  const app = express()

  if (!nuxtConfig.dev) {
    nuxtConfig.buildModules = []
  }
  // Init Nuxt.js
  const nuxt = new Nuxt(nuxtConfig)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (nuxtConfig.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }
  app.use(bodyParser.json())
  app.use('/api', require('./routes/api'))

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  // eslint-disable-next-line no-console
  console.log(`Server listening on http://${host}:${port}`)
}

module.exports = { start }
