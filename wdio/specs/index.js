/* global browser, $ */

describe('Pages > Index', function() {
  it('successfully logs in', () => {
    browser.url('/')

    const username = $('input[type=text]')
    username.setValue(process.env.TEST_USER_USERNAME)

    const password = $('input[type=password]')
    password.setValue(`${process.env.TEST_USER_PASSWORD}`)
    browser.keys('Enter')

    const list = $('tbody')
    expect(list).toHaveChildren({ gte: 2 })
  })
})
