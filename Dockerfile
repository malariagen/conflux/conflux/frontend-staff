FROM node:12-alpine AS base

RUN apk update \
  && apk add git

ENV PORT 5000
ENV HOST 0.0.0.0

EXPOSE $PORT
WORKDIR /app


FROM base AS base-production

COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm ci --production


FROM base-production AS build

ARG FONTAWESOME_NPM_AUTH_TOKEN
ENV FONTAWESOME_NPM_AUTH_TOKEN=$FONTAWESOME_NPM_AUTH_TOKEN

RUN npm config set "@fortawesome:registry" https://npm.fontawesome.com/
RUN npm config set "//npm.fontawesome.com/:_authToken" $FONTAWESOME_NPM_AUTH_TOKEN
RUN npm ci
COPY . /app
RUN npm run build


FROM base-production

ENV NODE_ENV=production
COPY . /app
COPY --from=build /app/.nuxt /app/.nuxt
CMD ["npm", "run", "start"]
