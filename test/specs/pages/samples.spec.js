import { shallowMount } from '@vue/test-utils'
import samples from '@/pages/samples.vue'
import ModelTable from '@/components/model-table/ModelTable'

describe('Pages > samples', () => {
  test('renders without errors', () => {
    const wrapper = shallowMount(samples)
    expect(wrapper.findComponent(ModelTable).exists()).toBe(true)
  })
})
