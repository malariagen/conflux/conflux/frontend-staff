import { mount } from '@vue/test-utils'
import index from '@/pages/index.vue'

describe('Pages > index', () => {
  test('redirects to samples', () => {
    const wrapper = mount(index)
    const redirect = jest.fn()
    // Manually run middleware function
    wrapper.vm.$options.middleware({ redirect })
    expect(redirect).toHaveBeenCalledWith('/studies')
  })
})
