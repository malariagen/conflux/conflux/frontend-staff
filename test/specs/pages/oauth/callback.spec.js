import { mount } from '@vue/test-utils'
import callback from '@/pages/oauth/callback.vue'

describe('Pages > oauth/callback', () => {
  test('shows error if no access token', () => {
    const wrapper = mount(callback)
    const redirect = jest.fn()
    // Manually run middleware function
    wrapper.vm.$options.middleware({ redirect })
    const error = wrapper.find('.text-danger')
    expect(error.text()).toBe('Error: no access token.')
    expect(redirect).toHaveBeenCalledTimes(0)
  })

  test('redirects if access token', () => {
    // Setup
    delete window.location
    window.location = { hash: '#access_token=test' }

    const wrapper = mount(callback)
    const redirect = jest.fn()
    // Manually run middleware function
    wrapper.vm.$options.middleware({ redirect })
    expect(redirect).toHaveBeenCalled()

    // Teardown
    window.location = location
  })
})
