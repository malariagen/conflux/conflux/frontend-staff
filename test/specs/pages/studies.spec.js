import { shallowMount } from '@vue/test-utils'
import studies from '@/pages/studies.vue'
import ModelTable from '@/components/model-table/ModelTable'

describe('Pages > studies', () => {
  test('renders without errors', () => {
    const wrapper = shallowMount(studies)
    expect(wrapper.findComponent(ModelTable).exists()).toBe(true)
  })
})
