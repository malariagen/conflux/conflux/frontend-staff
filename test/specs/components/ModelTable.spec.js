import { mount, createLocalVue } from '@vue/test-utils'
import SangerComponents from '@outlandish/sanger-components'
import ModelTable from '@/components/model-table/ModelTable'
const MOCK_MODEL = {
  fields: [
    {
      name: 'id',
      title: 'Study ID'
    },
    {
      name: 'name',
      title: 'Study Name'
    }
  ]
}

const MOCK_DATA = {
  rows: Array.from({ length: 100 }).map((_, i) => ({
    id: i,
    name: `Study ${i}`
  })),
  meta: {
    total_results: 10,
    total_filtered_results: 10,
    offset: 0,
    limit: 10,
    summary: []
  }
}

describe('Components > ModelTable', () => {
  let localVue
  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(SangerComponents)
  })
  test('fetches model', () => {
    // Setup
    const store = {
      state: {
        models: {}
      },
      dispatch: jest.fn()
    }

    // Test
    mount(ModelTable, {
      localVue,
      propsData: {
        modelName: 'study',
        modelPlural: 'studies'
      },
      mocks: {
        $store: store
      }
    })

    // wait for created hook
    expect(store.dispatch).toHaveBeenCalledWith('models/loadModel', 'study')
  })
  test('displays rows', async () => {
    // Setup
    const store = {
      state: {
        models: {
          study: MOCK_MODEL
        },
        studies: MOCK_DATA
      },
      dispatch: jest.fn()
    }

    // Test
    const wrapper = mount(ModelTable, {
      localVue,
      propsData: {
        modelName: 'study',
        modelPlural: 'studies'
      },
      mocks: {
        $store: store
      }
    })

    // Don't need to reload the model as it is in the store
    // Don't need to load rows as we already have 100
    expect(store.dispatch).not.toHaveBeenCalled()

    // Wait for getData() to resolve
    await wrapper.vm.$nextTick()
    // Wait for view to be updated
    await wrapper.vm.$nextTick()

    const summary = wrapper.findAll('tbody tr.summary')
    // No summary information
    expect(summary.length).toBe(0)
    // 100 rows of data
    const rows = wrapper.findAll('tbody tr')
    expect(rows.length).toBe(100)
  })
})
