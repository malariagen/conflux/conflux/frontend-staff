const config = require('./server/config')

module.exports = {
  mode: 'spa',
  srcDir: 'client',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Conflux Frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: config.description
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#4051ba' },
  /*
   ** Global CSS
   */
  css: [
    { src: '~/assets/app.scss', lang: 'scss' },
    '~/../node_modules/@outlandish/sanger-components/dist/sanger-components.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/sanger-components.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    hotMiddleware: {
      client: {
        // turn off client overlay when errors are present
        overlay: false
      }
    }
  },
  server: {
    host: config.host,
    port: config.port
  },
  router: {
    middleware: ['auth']
  },
  telemetry: false
}
